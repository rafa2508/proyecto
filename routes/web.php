<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/', 'DashController');
    Route::get('/logout')->name('logout')->uses('Auth\LoginController@logout');

    Route::resource('/recipe', 'RecipeController');
    Route::resource('/ingredient', 'IngredientController');
    Route::resource('/category', 'CategoryController');
});

